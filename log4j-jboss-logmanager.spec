%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                log4j-jboss-logmanager
Version:             1.3.0
Release:             2
Summary:             JBoss Log4j Emulation
License:             ASL 2.0
Url:                 https://github.com/jboss-logging/log4j-jboss-logmanager
Source0:             https://github.com/jboss-logging/log4j-jboss-logmanager/archive/%{namedversion}.tar.gz
BuildRequires:       maven-local mvn(junit:junit) mvn(log4j:log4j:12) jakarta-mail
BuildRequires:       mvn(org.apache.maven.plugins:maven-shade-plugin) maven-failsafe-plugin
BuildRequires:       mvn(org.jboss:jboss-parent:pom:) mvn(org.jboss.logging:jboss-logging)
BuildRequires:       mvn(org.jboss.logmanager:jboss-logmanager) mvn(org.jboss.modules:jboss-modules)
BuildRequires:       java-11-openjdk-devel
Requires:            java-11-openjdk
Requires:            javapackages-tools
Provides:            bundled(log4j12) = 1.2.17-15
BuildArch:           noarch
%description
This package contains the JBoss Log4J Emulation.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n log4j-jboss-logmanager-%{namedversion}
cp -p src/main/resources/META-INF/LICENSE .
cp -p src/main/resources/META-INF/NOTICE .
sed -i 's/\r//' LICENSE NOTICE
sed -i 's/createSourcesJar>true/createSourcesJar>false/' pom.xml
%pom_change_dep :log4j ::12

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%license LICENSE NOTICE

%files javadoc -f .mfiles-javadoc
%license LICENSE NOTICE

%changelog
* Tue Aug 01 2023 Ge Wang <wang__ge@126.com> - 1.3.0-2
- Compile with jdk11 due to jboss-modules updated

* Mon Jun 6 2022 liyanan <liyanan32@h-partners.com> - 1.3.0-1
- update to 1.3.0

* Sat Aug 22 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.1.2-1
- Package init
